from django.db import models


class Profile(models.Model):
    user = models.OneToOneField('auth.User', on_delete=models.CASCADE)

    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)

    followers = models.ManyToManyField('Profile', related_name='followings', through='Follower')

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    def follow(self, profile):
        self.followers.add(profile)

    def unfollow(self, profile):
        self.followers.remove(profile)


class Follower(models.Model):
    follower = models.ForeignKey('Profile', related_name='+', on_delete=models.CASCADE)
    following = models.ForeignKey('Profile', related_name='+', on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (('follower', 'following'),)
