from rest_framework import mixins
from rest_framework.decorators import detail_route, action

from utils import viewsets
from trackingcard.serializers import TrackCardSerializer
from .serializers import ProfileSerializer


class ProfileViewSet(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     mixins.UpdateModelMixin,
                     viewsets.GenericModelViewSet):
    serializer_class = ProfileSerializer

    def get_object(self):
        return self.request['user'].profile

    @action(detail=True, methods=['post'])
    def follow(self, pk, request):
        profile = self.get_object()
        profile.follow(request.profile)

    @action(detail=True, methods=['post'])
    def unfollow(self, pk, request):
        profile = self.get_object()
        profile.unfollow(request.profile)
