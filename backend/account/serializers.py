from rest_framework import serializers
from utils.serializers import ModelSerializer
from . import models


class ProfileSerializer(ModelSerializer):
    followers_count = serializers.IntegerField(read_only=True, source='followers.count')
    followings_count = serializers.IntegerField(read_only=True, source='followings.count')

    class Meta:
        model = models.Profile
        fields = '__all__'
