class ActionConstants(object):
    TYPE_LIKE = 1
    TYPE_FOLLOW = 2
    TYPE_NEW_TRACKCARD = 3

    TYPE_CHOICES = (
        (TYPE_LIKE, 'like'),
        (TYPE_FOLLOW, 'follow'),
        (TYPE_NEW_TRACKCARD, 'track card'),
    )