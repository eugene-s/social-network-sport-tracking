from django.db import models

import account.models
import trackingcard.models
from activities import constants


class ActionQuerySet(models.manager.QuerySet):
    def create_for_like(self, profile: account.models.Profile, target: trackingcard.models.TrackCard):
        data = {
            'profile': profile,
            'target_trackcard': target,
        }
        return self.create(**data)

    def create_for_follow(self, profile: account.models.Profile, target: account.models.Profile):
        data = {
            'profile': profile,
            'target_profile': target,
        }
        return self.create(**data)

    def create_for_trackcard(self, profile: account.models.Profile, target: trackingcard.models.TrackCard):
        data = {
            'profile': profile,
            'target_trackcard': target
        }
        return self.create(**data)


class Action(models.Model):
    CONSTANTS = constants.ActionConstants

    profile = models.ForeignKey('account.Profile', related_name='activities', on_delete=models.CASCADE)

    type = models.IntegerField(choices=CONSTANTS.TYPE_CHOICES)

    target_trackcard = models.ForeignKey('trackingcard.TrackCard', related_name='actions', on_delete=models.CASCADE,
                                         null=True, blank=True)
    target_profile = models.ForeignKey('account.Profile', related_name='actions', on_delete=models.CASCADE,
                                       null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)

    objects = ActionQuerySet.as_manager()
