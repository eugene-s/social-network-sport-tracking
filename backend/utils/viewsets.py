from rest_framework import viewsets, mixins


class GenericModelViewSet(viewsets.GenericViewSet):
    def get_queryset(self):
        return self.serializer_class.Meta.model.objects.get_queryset()


class ReadOnlyModelViewSet(mixins.RetrieveModelMixin,
                           mixins.ListModelMixin,
                           GenericModelViewSet):
    pass
