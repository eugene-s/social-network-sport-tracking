from rest_framework import permissions


class OwnProfileAccessOnlyPermission(permissions.DjangoModelPermissions):
    pass
