from rest_framework import serializers
from utils.serializers import ModelSerializer
from . import models


class TrackCardSerializer(ModelSerializer):
    likes_count = serializers.IntegerField(read_only=True, source='likes.count')

    class Meta:
        model = models.TrackCard
        fields = '__all__'


class CardTypeSerializer(ModelSerializer):
    trackcards_count = serializers.IntegerField(read_only=True, source='trackcards.count')

    class Meta:
        model = models.CardType
        fields = '__all__'
