from rest_framework import mixins
from rest_framework.decorators import action

from utils import viewsets
from utils.permissions import OwnProfileAccessOnlyPermission
from . import serializers


class TrackCardViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = serializers.TrackCardSerializer

    @action(detail=True, methods=['post'])
    def like(self, pk, request):
        trackcard = self.get_object()
        trackcard.like(request.profile)

    @action(detail=True, methods=['post'])
    def unlike(self, pk, request):
        trackcard = self.get_object()
        trackcard.unlike(request.profile)


class CardTypeViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = serializers.CardTypeSerializer
