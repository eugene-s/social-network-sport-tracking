from django.db import models


class CardType(models.Model):
    name = models.CharField(max_length=64)

    created_by = models.ForeignKey('account.Profile', related_name='cardtypes', on_delete=models.SET_NULL,
                                   null=True, blank=True)


class TrackCardLike(models.Model):
    profile = models.ForeignKey('account.Profile', on_delete=models.CASCADE)
    trackcard = models.ForeignKey('TrackCard', on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (('profile', 'trackcard'),)


class TrackCard(models.Model):
    profile = models.ForeignKey('account.Profile', related_name='trackcards', on_delete=models.CASCADE)

    type = models.ForeignKey('CardType', related_name='trackcards', on_delete=models.CASCADE)
    message = models.TextField(max_length=256)

    likes = models.ManyToManyField('account.Profile', related_name='likedtrackcards', through='TrackCardLike')

    created_at = models.DateTimeField(auto_now_add=True)

    def like(self, profile):
        self.likes.add(profile)

    def unlike(self, profile):
        self.likes.remove(profile)
