from rest_framework.routers import DefaultRouter
from .viewsets import TrackCardViewSet, CardTypeViewSet

router = DefaultRouter()

router.register('trackcard', TrackCardViewSet, 'trackcard')
router.register('cardtype', CardTypeViewSet, 'cardtype')

urlpatterns = router.urls