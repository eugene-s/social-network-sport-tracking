from django.apps import AppConfig


class TrackingcardConfig(AppConfig):
    name = 'trackingcard'
