"""sporttracking URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path
from rest_framework.documentation import include_docs_urls
from rest_framework_swagger.views import get_swagger_view

api_v1_url = [
    path(r'account/', include('account.urls')),
    path(r'trackingcard/', include('trackingcard.urls')),
]

api_url = [
    url(r'^auth/', include('rest_auth.urls')),
    path(r'v1/', (api_v1_url, 'v1', 'v1'))
]

urlpatterns = [
    path(r'admin/', admin.site.urls),
    path(r'api/', (api_url, 'api', 'api')),
    url(r'^docs/', include_docs_urls(title='My API title')),
    url(r'^swagger/', get_swagger_view(title='Pastebin API')),
]
