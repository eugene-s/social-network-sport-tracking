import * as storage from 'states/auth/session/storage';
import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';


export interface ActionState {
  token: string;
  name: string;
}


export function createInitialSessionState(): ActionState {
  return {
    token: null,
    name: null,
    ...storage.getSession()
  };
}


@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'action' })
export class ActionStore extends Store<ActionState> {
  constructor() {
    super(createInitialSessionState());
  }

  login(session: ActionState) {
    this.update(session);
    storage.saveSession(session);
  }

  logout() {
    storage.clearSession();
    this.update(createInitialSessionState());
  }
}
