import { ID } from '@datorama/akita';


export interface Action {
  id: ID;
}
