import * as storage from 'states/auth/session/storage';
import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';


export interface ProfileState {
  token: string;
  name: string;
}


export function createInitialSessionState(): ProfileState {
  return {
    token: null,
    name: null,
    ...storage.getSession()
  };
}


@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'accountProfile' })
export class ProfileStore extends Store<ProfileState> {
  constructor() {
    super(createInitialSessionState());
  }

  login(session: ProfileState) {
    this.update(session);
    storage.saveSession(session);
  }

  logout() {
    storage.clearSession();
    this.update(createInitialSessionState());
  }
}
