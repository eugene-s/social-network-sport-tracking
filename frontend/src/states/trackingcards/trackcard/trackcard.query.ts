import { TrackcardListState, TrackcardListStore } from 'states/trackingcards/trackcard/trackcard.store';
import { QueryEntity } from '@datorama/akita';
import { TrackcardList } from 'states/trackingcards/trackcard/trackcard.model';
import { Injectable } from '@angular/core';


@Injectable({ providedIn: 'root' })
export class TrackcardListQuery extends QueryEntity<TrackcardListState, TrackcardList> {
  constructor(
    protected store: TrackcardListStore
  ) {
    super(store);
  }
}
