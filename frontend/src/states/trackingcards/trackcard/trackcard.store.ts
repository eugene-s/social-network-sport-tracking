import { ActiveState, EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { TrackcardList } from './trackcard.model';
import { Injectable } from '@angular/core';


export interface TrackcardListState extends EntityState<TrackcardList>, ActiveState { }


@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'trackingcards-trackcard' })
export class TrackcardListStore extends EntityStore<TrackcardListState, TrackcardList> {
  constructor() {
    super();
  }
}
