import { ID } from '@datorama/akita';


export interface Trackcard {
  id?: ID;
  profile?: ID;

  type?: ID;
  message?: string;

  likes_count?: number;
  create_at?: Date;
}


export interface TrackcardList {
  items: Trackcard[];
}


export function createTrackcard(trackcard: Partial<Trackcard>) {
  return {
    ...trackcard
  };
}
