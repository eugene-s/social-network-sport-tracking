import { ID } from '@datorama/akita';


export interface Cardtype {
  id: ID;
}


export function createCardtype(cardtype: Partial<Cardtype>) {
  return {
    ...cardtype
  };
}
