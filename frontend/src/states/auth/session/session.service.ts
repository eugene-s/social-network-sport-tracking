import { SessionStore } from 'states/auth/session/session.store';
import { SessionDataService } from 'states/auth/session/session-data.service';
import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';


@Injectable({ providedIn: 'root' })
export class SessionService {
  constructor(
    private authStore: SessionStore,
    private authDataStore: SessionDataService,
  ) { }

  login(creds) {
    return this.authDataStore.login(creds).pipe(
      tap(session => this.authStore.login(session))
    );
  }

  logout() {
    this.authStore.logout();
  }
}
