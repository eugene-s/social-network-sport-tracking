import { Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { SessionQuery } from 'states/auth/session/session.query';


@Directive({
  selector: '[appShowIfLoggedIn]'
})
export class ShowIfLoggedInDirective implements OnInit, OnDestroy {
  @Input() showIfLoggedIn: boolean;

  subscription: Subscription;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private authQuery: SessionQuery,
  ) { }

  ngOnInit() {
    this.subscription = this.authQuery.isLoggedIn$
      .subscribe(isLoggedIn => {
        this.viewContainer.clear();
        if (isLoggedIn) {
          if (this.showIfLoggedIn) {
            this.viewContainer.createEmbeddedView(this.templateRef);
          } else {
            this.viewContainer.clear();
          }
        } else {
          if (this.showIfLoggedIn) {
            this.viewContainer.clear();
          } else {
            this.viewContainer.createEmbeddedView(this.templateRef);
          }
        }
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
