import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowIfLoggedInDirective } from './directives/show-if-logged-in.directive';
import { AuthGuard } from 'common/guards/auth.guard';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    ShowIfLoggedInDirective,
  ],
  providers: [
    AuthGuard,
  ]
})
export class AppCommonModule { }
