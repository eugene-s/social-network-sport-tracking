import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrackingcardTrackcardComponent } from './trackingcard-trackcard/trackingcard-trackcard.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
  ],
  declarations: [
    TrackingcardTrackcardComponent,
  ],
  exports: [
    TrackingcardTrackcardComponent,
  ]
})
export class TrackingcardComponentsModule { }
