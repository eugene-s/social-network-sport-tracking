import { Component, Input, OnInit } from '@angular/core';
import { Trackcard } from 'states/trackingcards/trackcard/trackcard.model';

@Component({
  selector: 'app-trackingcard-trackcard',
  templateUrl: './trackingcard-trackcard.component.html',
  styleUrls: ['./trackingcard-trackcard.component.scss']
})
export class TrackingcardTrackcardComponent implements OnInit {
  @Input() trackcard: Trackcard;

  constructor() { }

  ngOnInit() {
  }

}
