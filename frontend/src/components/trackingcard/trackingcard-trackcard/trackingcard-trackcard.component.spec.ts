import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackingcardTrackcardComponent } from './trackingcard-trackcard.component';

describe('TrackingcardTrackcardComponent', () => {
  let component: TrackingcardTrackcardComponent;
  let fixture: ComponentFixture<TrackingcardTrackcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackingcardTrackcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackingcardTrackcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
