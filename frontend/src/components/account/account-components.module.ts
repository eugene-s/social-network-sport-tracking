import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountProfileComponent } from './account-profile/account-profile.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AccountProfileComponent, AccountSettingsComponent]
})
export class AccountComponentsModule { }
