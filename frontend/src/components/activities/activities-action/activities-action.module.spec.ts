import { ActivitiesActionModule } from './activities-action.module';

describe('ActivitiesActionModule', () => {
  let activitiesActionModule: ActivitiesActionModule;

  beforeEach(() => {
    activitiesActionModule = new ActivitiesActionModule();
  });

  it('should create an instance', () => {
    expect(activitiesActionModule).toBeTruthy();
  });
});
