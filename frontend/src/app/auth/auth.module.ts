import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthLoginComponent } from './auth-login/auth-login.component';
import { AuthRegisterComponent } from './auth-register/auth-register.component';
import { AuthComponent } from './auth.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild([
      { path: 'login', component: AuthLoginComponent },
      { path: 'signup', component: AuthRegisterComponent },
    ]),
  ],
  declarations: [
    AuthLoginComponent,
    AuthRegisterComponent,
    AuthComponent,
  ]
})
export class AuthModule { }
