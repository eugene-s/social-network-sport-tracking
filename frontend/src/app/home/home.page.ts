import { Component } from '@angular/core';
import { Trackcard } from 'states/trackingcards/trackcard/trackcard.model';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  trackcardList: Trackcard[] = [
    {message: 'First Message 1'},
    {message: 'First Message 1'},
    {message: 'First Message 1'},
    {message: 'First Message 1'},
    {message: 'First Message 1'},
    {message: 'First Message 1'},
    {message: 'First Message 1'},
    {message: 'First Message 1'},
  ];
}
