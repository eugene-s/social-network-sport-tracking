import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuard } from 'common/guards/auth.guard';


@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', loadChildren: './auth/auth.module#AuthModule' },
      { path: '', loadChildren: './tabs/tabs.module#TabsPageModule',
        canActivate: [ AuthGuard ] },
    ])
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule {}
