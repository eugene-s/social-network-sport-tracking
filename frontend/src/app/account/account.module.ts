import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountComponent } from './account.component';
import { AccountComponentsModule } from 'components/account/account-components.module';
import { IonicModule } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    AccountComponentsModule,
    IonicModule,
  ],
  declarations: [AccountComponent]
})
export class AccountModule { }
